import csv
import json
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import numpy as np
from inputDataReaderII import *


def doMakeRAMSESRateFile(ConfigInfo):

    AnalysisDir   = ConfigInfo['AnalysisDir']
    InputScanFile = AnalysisDir + "/" + ConfigInfo['InputScanFile']
    InputBeamCurrentFile = AnalysisDir + "/" + ConfigInfo['InputBeamCurrentFile']
    DataType      = ConfigInfo['RateTable']
    muscan = ConfigInfo['MuScan']
    if DataType == "ramses5514lumi":
        InputRamsesFiles = ConfigInfo["InputRAMSES5514Files"]
    elif DataType == "ramses5515lumi":
        InputRamsesFiles = ConfigInfo["InputRAMSES5515Files"]
    elif DataType == "ramseslumi":
	InputRamsesFiles = ConfigInfo["InputRAMSES5514Files"] + ConfigInfo["InputRAMSES5515Files"]

    vdmData = vdmInputData(1)
    vdmData.GetScanInfo(InputScanFile)
    vdmData.GetBeamCurrentsInfo(InputBeamCurrentFile)
     
    table={"Scan_1":[],"Scan_2":[]}
    if muscan:
	table={"Scan_1":[]}
    
    #print vdmData
    for bx in vdmData.collidingBunches:
	bcid=str(bx) 
	break

    with open(InputScanFile,'r') as f:
	scanInfo= json.load(f)

    scanX=[]
    scanY=[]
    RamsesData={}
    ss=1
    if muscan:
	ss=0
    for fl in InputRamsesFiles:
	with open(fl, 'r') as data_csv:
		data = csv.reader(data_csv)

		for line in data:
			if long(line[0].split(".")[0]) >= long(scanInfo["ScanTimeWindows"][0][0]) and long(line[0].split(".")[0]) <= long(scanInfo["ScanTimeWindows"][1][1]):
				if line[0] in RamsesData.keys():
					nr = float(line[1])+ float(RamsesData[line[0]])
					RamsesData[line[0]]=nr/2
				else:
					RamsesData[line[0]]=line[1]
    arr=["Scan_1","Scan_2"]
    if muscan:
        arr=["Scan_1"] 
    for i in arr:
	for j,k in enumerate(scanInfo[i]):
		if i=='Scan_1':
			scanX.append([k[3]])
			scanX[j].append(k[4])
		elif i=='Scan_2':
                        scanY.append([k[3]])
                        scanY[j].append(k[4])

    scan={"Scan_1":[scanX,"X1"],"Scan_2":[scanY,"Y1"]}
    if muscan:
        scan={"Scan_1":[scanX,"X1"]}
    for i in scan.keys():
	#print i
	TimeALL=[]
	RateALL=[]
        for j,k in enumerate(scan[i][0]):
		#print j
		time=[]
		rate=[]
		for T, R in RamsesData.items():
			timestamp = T.split(".")[0]
			#print timestamp,scanBeginX
			if long(timestamp) > long(k[0])  and long(timestamp) < long(k[1]):
				time.append(long(timestamp))
				rate.append(float(R))

		TimeALL.extend(time)
                RateALL.extend(rate)		

		np.asarray(rate)
		rateErr, rateAve = np.std(rate), np.mean(rate)

		table[i].append({"RateErrs":{bcid:rateErr},"Rates":{bcid:rateAve},"ScanNumber":int(i.split('_')[1]),"ScanName":scan[i][1],"ScanPoint":j+1})

	'''
	plt.errorbar(TimeALL, RateALL, yerr=0, fmt='o', markersize=7)
	plt.grid()
	plt.savefig( './'+scan[i][1]+'.pdf')
	plt.clf()
	'''
    return table
    #with open(OutputSubDir+'/Rates_' + Luminometer +  '_'+str(Fill)+'.json', 'wb') as f:
    #with open('./Rates_RAMSES_7020.json', 'wb') as ff:
    #            json.dump(table, ff)




