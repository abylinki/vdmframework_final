import sys
import json
import pickle

jsonFileName=sys.argv[1]
pickleFileName=jsonFileName.replace("json","pkl")

jsonFile=open(jsonFileName)
pickleFile=open(pickleFileName,"w")

jsonDict=json.load(jsonFile)

pickle.dump(jsonDict,pickleFile)

pickleFile.close()
jsonFile.close()

