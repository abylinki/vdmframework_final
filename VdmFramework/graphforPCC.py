import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
import json
import ROOT as r
'''
bxs=["62","109","333"]
#fi="/scratch/yusufcan_VdM/VdmFramework/Automation/Analysed_Data/7274_11Oct18_015227_11Oct18_021602/LuminometerData/Rates_PCC_7274.json"
fi="/scratch/yusufcan_VdM/VdmFramework/Automation/Analysed_Data/7217_25Sep18_001550_25Sep18_001833/cond/BeamCurrents_7217.json"
with open(fi) as f:
	data = json.load(f)
	#for bx in bxs:
	x=[]
	y=[]
	for bx in data['Scan_1'][0]['fbctB1'].keys()
		#x=[]
		#y=[]
		#e=[]
		for d in data['Scan_1']:
			x.append(d["ScanPoint"])
			y.append(d["Rates"][bx])
			e.append(d["RateErrs"][bx])
		plt.errorbar(x, y, yerr=e, fmt='o')
                plt.title("PCC_Rates for BCID_"+bx)
		plt.xlabel("LS")
		plt.ylabel("Rates") 

		plt.savefig("./plots/bx"+bx+".pdf")
		plt.clf()
'''

'''
f = r.TFile("fromBrilcalc.root")
t = f.Get("pccminitree")
#t.Print()
l = t.GetLeaf("LS")
l.Print()
'''

fi="/scratch/yusufcan_VdM/VdmFramework/Automation/Analysed_Data/7217_25Sep18_001550_25Sep18_001833/cond/BeamCurrents_7217.json"
with open(fi) as f:
        data = json.load(f)
        #for bx in bxs:
        x=[]
        y=[]
        for bx in data['Scan_1'][0]['fbctB1'].keys():
		if bx == 'sum':
			continue
		else:
        		x.append(int(bx))
                	y.append(data['Scan_1'][0]['fbctB1'][bx])
        plt.errorbar(x, y, yerr=0, fmt='o')
        plt.title("FBCT versus BX")
        plt.xlabel("BX")
        plt.ylabel("FBCT")

        plt.savefig("./plots/fbct.pdf")
        plt.clf()
